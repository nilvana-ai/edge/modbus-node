# How to use
[[中文]](README_zh-TW.md)[[English]](README.md)

- [Modbus-RTU](#modbus-rtu)
- [Modbus-TCP](#modbus-tcp)

## Modbus-RTU

### 1. Connect the device that you want to use into Atom N2 serial port

Refer to the figure below to connect the Modbus device
![](../images/N2_modbus.jpg)

### 2. Enter `nilvana flow` website

nilvana flow [http://<YOUR_Atom_IP>:1880/](http://<YOUR_Atom_IP>:1880/)

![](../images/nilvana-flow.png)

### 3. Pull out the `modbus editor` node to operate your Modbus device

Double-click on the `modbus editor` node

![](../images/modbus-editor-prop.png)

- Server 

> Choose or edit the Modbus serial server configuration to specify the server to connect to.
Click the server settings button ![](../images/edit-button.png) to set the `baud rate` and other settings of your device.
please select the default `/dev/ttyTHS0` that is the built-in serial port of Atom N2 modbus

- Unit-Id 
> Enter the ID of your device (range 0~247)

- Function Code
> Choose a function code (FC)1, 2, 3, 4, 5, 6, 15 ,16 from the dropdown menu

    - FC 1: Read Coil Status
    - FC 2: Read Input Status
    - FC 3: Read Holding Registers
    - FC 4: Read Input Registers
    - FC 5: Force Single Coil
    - FC 6: Preset Single Register
    - FC 15: Force Multiple Coils
    - FC 16: Preset Multiple Registers
> For FC 5, msg.payload must be a value of 1 or 0 or true or false.

> For FC 6, msg.payload must be a single value between 0~65535.

> For FC 15, msg.payload must be an array[] of comma separated values true or false each.

> For FC 16, msg.payload must be an array[] of comma separated values between 0~65535 each.

- Address
> start address to read/write. (range 0~65535)

- Quantity

> the count of items start from to be read/write

### 4. Modbus RTU flow example

4.1. import example flow

Click the `import` button from the `menu` button in the upper right corner
![](../images/import-flow1.png)

Choose to import the [json file](modbus-RTU.json) or paste the content of the json file and click the `import` button
![](../images/import-flow2.png)

you will see the flow like picture below
![](../images/import-flow3.png)

## Modbus-TCP

### 1. Enter `nilvana flow` website

nilvana flow [http://<YOUR_Atom_IP>:1880/](http://<YOUR_Atom_IP>:1880/)

### 2. Pull out the `modbus editor` node to operate your Modbus TCP server

Double-click on the `modbus editor` node

![](../images/modbus-editor-prop.png)

- Server 

> Choose or edit the Modbus TCP server configuration to specify the server to connect to.
Click the server settings button ![](../images/edit-button.png) to set the `IP` and `Port`

- Unit-Id 
> Enter the ID of your device (range 0~247)

- Function Code
> Choose a function code (FC)1, 2, 3, 4, 5, 6, 15 ,16 from the dropdown menu

    - FC 1: Read Coil Status
    - FC 2: Read Input Status
    - FC 3: Read Holding Registers
    - FC 4: Read Input Registers
    - FC 5: Force Single Coil
    - FC 6: Preset Single Register
    - FC 15: Force Multiple Coils
    - FC 16: Preset Multiple Registers
> For FC 5, msg.payload must be a value of 1 or 0 or true or false.

> For FC 6, msg.payload must be a single value between 0~65535.

> For FC 15, msg.payload must be an array[] of comma separated values true or false each.

> For FC 16, msg.payload must be an array[] of comma separated values between 0~65535 each.

- Address
> start address to read/write. (range 0~65535)

- Quantity

> start address to read/write. (range 0~65535)

### 3. Modbus TCP flow example

3.1. import example flow

Click the `import` button from the `menu` button in the upper right corner
![](../images/import-flow1.png)

Choose to import the [json file](modbus-TCP.json) or paste the content of the json file and click the `import` button
![](../images/import-flow2.png)

you will see the flow like picture below
![](../images/import-flow3.png)

