# How to use
[[中文]](README_zh-TW.md)[[English]](README.md)

- [Modbus-RTU](#modbus-rtu)
- [Modbus-TCP](#modbus-tcp)

## Modbus-RTU

### 1. 接上 想連接的 `modbus` 裝置到`Atom N2`的序列埠

參考下圖連接您的Modbus裝置
![](../images/N2_modbus.jpg)

### 2. 進入 `nilvana flow` 網頁

nilvana flow [http://<YOUR_Atom_IP>:1880/](http://<YOUR_Atom_IP>:1880/)

![](../images/nilvana-flow.png)

### 3. 拉出 `modbus editor` node 來操作您的裝置

在`modbus editor` node右鍵雙擊兩下

![](../images/modbus-editor-prop.png)

- Server 

> 選擇或編輯配置特定的server連接. 點選 server設定按鈕 ![](../images/edit-button.png) 設定您裝置的`baud rate`與其他設定

請選擇預設的序列埠`/dev/ttyTHS0`, 此為N2 modbus 內建的serial序列埠位置

- Unit-Id 
> 輸入裝置的ID (範圍 0~247)

- Function Code
> 下拉式選單選擇 function code (FC)1, 2, 3, 4, 5, 6, 15 ,16
    - FC 1: Read Coil Status
    - FC 2: Read Input Status
    - FC 3: Read Holding Registers
    - FC 4: Read Input Registers
    - FC 5: Force Single Coil
    - FC 6: Preset Single Register
    - FC 15: Force Multiple Coils
    - FC 16: Preset Multiple Registers
> For FC 5, msg.payload must be a value of 1 or 0 or true or false.

> For FC 6, msg.payload must be a single value between 0~65535.

> For FC 15, msg.payload must be an array[] of comma separated values true or false each.

> For FC 16, msg.payload must be an array[] of comma separated values between 0~65535 each.

- Address
> 讀/寫的起始位址 (範圍 0~65535)

- Quantity

> 由起始位址記算的數量

### 4. 詳細操作參考RTU flow範例

4.1. 匯入 範例example flow

從右上角 `選單` 進入 `import`
![](../images/import-flow1.png)

選擇使用匯入[json檔]((modbus-RTU.json)或是直接貼上json檔的內容 並 按下`import`按鈕
![](../images/import-flow2.png)
範例的flow 如下圖所示
![](../images/import-flow3.png)

## Modbus-TCP

### 1. 進入 `nilvana flow` 網頁

nilvana flow [http://<YOUR_Atom_IP>:1880/](http://<YOUR_Atom_IP>:1880/)

![](../images/nilvana-flow.png)

### 2. 拉出 `modbus editor` node 來操作您的裝置

在`modbus editor` node右鍵雙擊兩下

![](../images/modbus-editor-prop.png)

- Server 

> 選擇或編輯配置特定的Modbus TCP server連接. 點選 server設定按鈕 ![](../images/edit-button.png) 設定您要連接Modbus TCP server的`IP` 和 `Port`

- Unit-Id 
> 輸入裝置的ID (範圍 0~247)

- Function Code
> 下拉式選單選擇 function code (FC)1, 2, 3, 4, 5, 6, 15 ,16
    - FC 1: Read Coil Status
    - FC 2: Read Input Status
    - FC 3: Read Holding Registers
    - FC 4: Read Input Registers
    - FC 5: Force Single Coil
    - FC 6: Preset Single Register
    - FC 15: Force Multiple Coils
    - FC 16: Preset Multiple Registers
> For FC 5, msg.payload 必須是一個 1, 0, true, false的值

> For FC 6, msg.payload 必須是一個介於0~65535的值

> For FC 15, msg.payload 必須是個使用逗號分隔true或false的 array[]

> For FC 16, msg.payload 必須是個使用逗號分隔且值在0~66535的 array[]

- Address
> 讀/寫的起始位址 (範圍 0~65535)

- Quantity

> 由起始位址記算的數量

### 3. 詳細操作參考TCP flow範例

3.1. 匯入 範例example flow

從右上角 `選單` 進入 `import`
![](../images/import-flow1.png)

選擇使用匯入[json 檔](modbus-TCP.json)或是直接貼上json檔的內容 並 按下`import`按鈕
![](../images/import-flow2.png)
範例的flow 如下圖所示
![](../images/import-flow3.png)
