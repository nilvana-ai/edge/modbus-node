/**
 * Modbus Editor node.
 * @module NodeRedModbusEditor
 *
 * @param RED
 */
module.exports = function (RED) {
  'use strict'
  // SOURCE-MAP-REQUIRED
  const mbBasics = require('./modbus-basics')
  const mbCore = require('./core/modbus-core')
  const mbIOCore = require('./core/modbus-io-core')
  const internalDebugLog = require('debug')('contribModbus:editor')
  const readMode=0
  const writeMode=1

  function GetMode(dataType){
    switch(dataType){
      case 'R_Coil':
      case 'R_Input':
      case 'R_HoldingRegister':
      case 'R_InputRegister':
        return readMode
      case 'W_Coil':
      case 'W_HoldingRegister':
      case 'W_MCoils':
      case 'W_MHoldingRegisters':
        return writeMode
      default:
        console.log("not support", dataType)
        return -1
    }    
  }

  function ModbusEditor (config) {
    RED.nodes.createNode(this, config)

    this.name = config.name

    this.unitid = config.unitid
    this.dataType = config.dataType
    this.adr = Number(config.adr)
    this.quantity = config.quantity

    this.showStatusActivities = config.showStatusActivities
    this.showErrors = config.showErrors

    this.emptyMsgOnFail = config.emptyMsgOnFail
    this.keepMsgProperties = config.keepMsgProperties
    this.internalDebugLog = internalDebugLog
    this.verboseLogging = RED.settings.verbose

    const node = this

    node.bufferMessageList = new Map()

    mbBasics.setNodeStatusTo('waiting', node)

    const modbusClient = RED.nodes.getNode(config.server)
    if (!modbusClient) {
      return
    }
    modbusClient.registerForModbus(node)
    mbBasics.initModbusClientEvents(node, modbusClient)
    node.errorProtocolMsg = function (err, msg) {
      mbBasics.logMsgError(node, err, msg)
      mbBasics.sendEmptyMsgOnFail(node, err, msg)
    }

    if (GetMode(node.dataType) == writeMode){      
      node.onModbusWriteDone = function (resp, msg) {        
        if (node.showStatusActivities) {
          mbBasics.setNodeStatusTo('write done', node)
        }
        node.send(mbCore.buildMessage(node.bufferMessageList, msg.payload, resp, msg))
        node.emit('modbusWriteNodeDone')
      }

      node.onModbusWriteError = function (err, msg) {
        node.internalDebugLog(err.message)
        const origMsg = mbCore.getOriginalMessage(node.bufferMessageList, msg)
        node.errorProtocolMsg(err, origMsg)
        mbBasics.setModbusError(node, modbusClient, err, origMsg)
        node.emit('modbusWriteNodeError')
      }

      node.setMsgPayloadFromHTTPRequests = function (msg) {
        /* HTTP requests for boolean and multiple data string [1,2,3,4,5] */
        if (Object.prototype.hasOwnProperty.call(msg.payload, 'value') &&
          typeof msg.payload.value === 'string') {
          if (msg.payload.value === 'true' || msg.payload.value === 'false') {
            msg.payload.value = (msg.payload.value === 'true')
          } else {
            if (msg.payload.value.indexOf(',') > -1) {
              msg.payload.value = JSON.parse(msg.payload.value)
            }
          }
        }
        return msg
      }

      node.buildNewMessageObject = function (node, msg) {
        const messageId = mbCore.getObjectId()
        return {
          topic: msg.topic || node.id,
          messageId,
          payload: {
            value: (Object.prototype.hasOwnProperty.call(msg.payload, 'value')) ? msg.payload.value : msg.payload,
            unitid: node.unitid,
            fc: mbCore.functionCodeModbus(node.dataType),
            address: node.adr,
            quantity: node.quantity,
            messageId
          }
        }
      }

      node.on('input', function (msg) {
        const origMsgInput = Object.assign({}, msg)

        if (mbBasics.invalidPayloadIn(msg)) {
          return
        }

        if (!modbusClient.client) {
          return
        }

        try {
          const httpMsg = node.setMsgPayloadFromHTTPRequests(origMsgInput)
          const newMsg = node.buildNewMessageObject(node, httpMsg)
          node.bufferMessageList.set(newMsg.messageId, mbBasics.buildNewMessage(node.keepMsgProperties, httpMsg, newMsg))
          modbusClient.emit('writeModbus', newMsg, node.onModbusWriteDone, node.onModbusWriteError)

          if (node.showStatusActivities) {
            mbBasics.setNodeStatusTo(modbusClient.actualServiceState, node)
          }
        } catch (err) {
          node.errorProtocolMsg(err, origMsgInput)
        }
      })

      node.on('close', function (done) {
        mbBasics.setNodeStatusTo('closed', node)
        node.bufferMessageList.clear()
        modbusClient.deregisterForModbus(node.id, done)
      })

      
    }else if (GetMode(node.dataType) == readMode){      
      node.onModbusReadDone = function (resp, msg) {        
        if (node.showStatusActivities) {
          mbBasics.setNodeStatusTo('reading done', node)
        }
        node.send(mbIOCore.buildMessageWithIO(node, resp.data, resp, msg))
        node.emit('modbusEditorNodeDone')
      }
  
      node.errorProtocolMsg = function (err, msg) {
        mbBasics.logMsgError(node, err, msg)
        mbBasics.sendEmptyMsgOnFail(node, err, msg)
      }
  
      node.onModbusReadError = function (err, msg) {
        node.internalDebugLog(err.message)
        const origMsg = mbCore.getOriginalMessage(node.bufferMessageList, msg)
        node.errorProtocolMsg(err, origMsg)
        mbBasics.setModbusError(node, modbusClient, err, origMsg)
        node.emit('modbusGetterNodeError')
      }
  
      node.buildNewMessageObject = function (node, msg) {
        const messageId = mbCore.getObjectId()
        return {
          topic: msg.topic || node.id,
          messageId,
          payload: {
            value: msg.payload.value || msg.payload,
            unitid: node.unitid,
            fc: mbCore.functionCodeModbus(node.dataType),
            address: node.adr,
            quantity: node.quantity,
            messageId
          }
        }
      }
  
      node.on('input', function (msg) {
        if (mbBasics.invalidPayloadIn(msg)) {
          return
        }
  
        if (!modbusClient.client) {
          return
        }
  
        const origMsgInput = Object.assign({}, msg) // keep it origin
        try {
          const newMsg = node.buildNewMessageObject(node, origMsgInput)
          node.bufferMessageList.set(newMsg.messageId, mbBasics.buildNewMessage(node.keepMsgProperties, origMsgInput, newMsg))
          modbusClient.emit('readModbus', newMsg, node.onModbusReadDone, node.onModbusReadError)
  
          if (node.showStatusActivities) {
            mbBasics.setNodeStatusTo(modbusClient.actualServiceState, node)
          }
        } catch (err) {
          node.errorProtocolMsg(err, origMsgInput)
        }
      })
  
      node.on('close', function (done) {
        mbBasics.setNodeStatusTo('closed', node)
        node.bufferMessageList.clear()
        modbusClient.deregisterForModbus(node.id, done)
      })

    }else{
      console.log("never be here")
    }

    if (!node.showStatusActivities) {
      mbBasics.setNodeDefaultStatus(node)
    }
  }

  RED.nodes.registerType('modbus-editor', ModbusEditor)
}
